<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToDetalleTareasTable extends Migration
{
    public function up()
    {
        Schema::table('detalle_tareas', function (Blueprint $table) {
            $table->unsignedBigInteger('materia_id')->nullable();
            $table->foreign('materia_id', 'materia_fk_4854390')->references('id')->on('materia');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id', 'created_by_fk_4854395')->references('id')->on('users');
        });
    }
}
