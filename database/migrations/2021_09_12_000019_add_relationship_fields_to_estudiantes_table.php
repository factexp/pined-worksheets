<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToEstudiantesTable extends Migration
{
    public function up()
    {
        Schema::table('estudiantes', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id')->nullable();
            $table->foreign('usuario_id', 'usuario_fk_4849869')->references('id')->on('users');
            $table->unsignedBigInteger('curso_id')->nullable();
            $table->foreign('curso_id', 'curso_fk_4849870')->references('id')->on('cursos');
        });
    }
}
