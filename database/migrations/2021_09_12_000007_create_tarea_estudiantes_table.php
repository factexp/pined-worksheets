<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTareaEstudiantesTable extends Migration
{
    public function up()
    {
        Schema::create('tarea_estudiantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('valor')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
