<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRepresentantesTable extends Migration
{
    public function up()
    {
        Schema::table('representantes', function (Blueprint $table) {
            $table->unsignedBigInteger('estudiante_id')->nullable();
            $table->foreign('estudiante_id', 'estudiante_fk_4849875')->references('id')->on('users');
            $table->unsignedBigInteger('representante_id')->nullable();
            $table->foreign('representante_id', 'representante_fk_4849876')->references('id')->on('users');
        });
    }
}
