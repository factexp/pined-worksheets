<?php

namespace App\Http\Requests;

use App\Models\Tarea;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyTareaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('tarea_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:tareas,id',
        ];
    }
}
