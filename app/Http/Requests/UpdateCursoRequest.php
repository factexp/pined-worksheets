<?php

namespace App\Http\Requests;

use App\Models\Curso;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCursoRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('curso_edit');
    }

    public function rules()
    {
        return [
            'nombre' => [
                'string',
                'nullable',
            ],
        ];
    }
}
