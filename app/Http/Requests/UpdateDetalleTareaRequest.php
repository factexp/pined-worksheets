<?php

namespace App\Http\Requests;

use App\Models\DetalleTarea;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateDetalleTareaRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('detalle_tarea_edit');
    }

    public function rules()
    {
        return [];
    }
}
