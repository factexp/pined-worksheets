<?php

namespace App\Http\Requests;

use App\Models\Representante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRepresentanteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('representante_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:representantes,id',
        ];
    }
}
