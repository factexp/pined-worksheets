<?php

namespace App\Http\Requests;

use App\Models\Estudiante;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreEstudianteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('estudiante_create');
    }

    public function rules()
    {
        return [];
    }
}
