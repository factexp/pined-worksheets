<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyTareaRequest;
use App\Http\Requests\StoreTareaRequest;
use App\Http\Requests\UpdateTareaRequest;
use App\Models\Curso;
use App\Models\Materium;
use App\Models\Tarea;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class TareasController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('tarea_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tareas = Tarea::with(['profesor', 'materia', 'curso', 'created_by', 'media'])->get();

        return view('admin.tareas.index', compact('tareas'));
    }

    public function create(Request $request) 
    {
        abort_if(Gate::denies('tarea_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $materias = Materium::where('curso_id', $request->curso_id)->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');
        return view('admin.tareas.create', compact('materias', 'cursos'));
    }

    public function store(StoreTareaRequest $request)
    {
        $tarea = Tarea::create(array_merge($request->all(), ['profesor_id' => auth()->user()->id ]));

        if ($request->input('archivo', false)) {
            $tarea->addMedia(storage_path('tmp/uploads/' . basename($request->input('archivo'))))->toMediaCollection('archivo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $tarea->id]);
        }

        return redirect()->route('admin.tareas.index');
    }

    public function edit(Tarea $tarea)
    {
        abort_if(Gate::denies('tarea_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');        

        $materias = Materium::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cursos = Curso::pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $tarea->load('profesor', 'materia', 'curso', 'created_by');

        return view('admin.tareas.edit', compact('materias', 'cursos', 'tarea'));
    }

    public function update(UpdateTareaRequest $request, Tarea $tarea)
    {
        $tarea->update(array_merge($request->all(), ['profesor_id' => auth()->user()->id ]));

        if ($request->input('archivo', false)) {
            if (!$tarea->archivo || $request->input('archivo') !== $tarea->archivo->file_name) {
                if ($tarea->archivo) {
                    $tarea->archivo->delete();
                }
                $tarea->addMedia(storage_path('tmp/uploads/' . basename($request->input('archivo'))))->toMediaCollection('archivo');
            }
        } elseif ($tarea->archivo) {
            $tarea->archivo->delete();
        }

        return redirect()->route('admin.tareas.index');
    }

    public function show(Tarea $tarea)
    {
        abort_if(Gate::denies('tarea_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tarea->load('profesor', 'materia', 'curso', 'created_by');

        return view('admin.tareas.show', compact('tarea'));
    }

    public function destroy(Tarea $tarea)
    {
        abort_if(Gate::denies('tarea_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tarea->delete();

        return back();
    }

    public function massDestroy(MassDestroyTareaRequest $request)
    {
        Tarea::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('tarea_create') && Gate::denies('tarea_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Tarea();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
    
    public function showCursos()
    {
        abort_if(Gate::denies('tarea_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cursos = Curso::with(['created_by'])->get();

        return view('admin.tareas.showcourses', compact('cursos'));
    }
}
