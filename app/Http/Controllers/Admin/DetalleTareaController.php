<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyDetalleTareaRequest;
use App\Http\Requests\StoreDetalleTareaRequest;
use App\Http\Requests\UpdateDetalleTareaRequest;
use App\Models\DetalleTarea;
use App\Models\Materium;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DetalleTareaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('detalle_tarea_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalleTareas = DetalleTarea::with(['materia', 'created_by'])->get();

        $materia = Materium::get();

        $users = User::get();

        return view('admin.detalleTareas.index', compact('detalleTareas', 'materia', 'users'));
    }

    public function create()
    {
        abort_if(Gate::denies('detalle_tarea_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.detalleTareas.create');
    }

    public function store(StoreDetalleTareaRequest $request)
    {
        $detalleTarea = DetalleTarea::create($request->all());

        return redirect()->route('admin.detalle-tareas.index');
    }

    public function edit(DetalleTarea $detalleTarea)
    {
        abort_if(Gate::denies('detalle_tarea_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalleTarea->load('materia', 'created_by');

        return view('admin.detalleTareas.edit', compact('detalleTarea'));
    }

    public function update(UpdateDetalleTareaRequest $request, DetalleTarea $detalleTarea)
    {
        $detalleTarea->update($request->all());

        return redirect()->route('admin.detalle-tareas.index');
    }

    public function show(DetalleTarea $detalleTarea)
    {
        abort_if(Gate::denies('detalle_tarea_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalleTarea->load('materia', 'created_by');

        return view('admin.detalleTareas.show', compact('detalleTarea'));
    }

    public function destroy(DetalleTarea $detalleTarea)
    {
        abort_if(Gate::denies('detalle_tarea_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalleTarea->delete();

        return back();
    }

    public function massDestroy(MassDestroyDetalleTareaRequest $request)
    {
        DetalleTarea::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
