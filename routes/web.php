<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // Cursos
    Route::delete('cursos/destroy', 'CursosController@massDestroy')->name('cursos.massDestroy');
    Route::resource('cursos', 'CursosController');

    // Materia
    Route::delete('materia/destroy', 'MateriaController@massDestroy')->name('materia.massDestroy');
    Route::resource('materia', 'MateriaController');

    // Tareas
    Route::delete('tareas/destroy', 'TareasController@massDestroy')->name('tareas.massDestroy');
    Route::post('tareas/media', 'TareasController@storeMedia')->name('tareas.storeMedia');
    Route::post('tareas/ckmedia', 'TareasController@storeCKEditorImages')->name('tareas.storeCKEditorImages');
    Route::get('tareas/mostrarcursos', 'TareasController@showCursos')->name('tareas.mostrarcursos');
    Route::resource('tareas', 'TareasController');
    

    // Tarea Estudiante
    Route::delete('tarea-estudiantes/destroy', 'TareaEstudianteController@massDestroy')->name('tarea-estudiantes.massDestroy');
    Route::resource('tarea-estudiantes', 'TareaEstudianteController');

    // Estudiante
    Route::delete('estudiantes/destroy', 'EstudianteController@massDestroy')->name('estudiantes.massDestroy');
    Route::resource('estudiantes', 'EstudianteController');

    // Representante
    Route::delete('representantes/destroy', 'RepresentanteController@massDestroy')->name('representantes.massDestroy');
    Route::resource('representantes', 'RepresentanteController');

    // Detalle Tarea
    Route::delete('detalle-tareas/destroy', 'DetalleTareaController@massDestroy')->name('detalle-tareas.massDestroy');
    Route::resource('detalle-tareas', 'DetalleTareaController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
