@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.estudiante.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.estudiantes.update", [$estudiante->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('usuario') ? 'has-error' : '' }}">
                            <label for="usuario_id">{{ trans('cruds.estudiante.fields.usuario') }}</label>
                            <select class="form-control select2" name="usuario_id" id="usuario_id">
                                @foreach($usuarios as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('usuario_id') ? old('usuario_id') : $estudiante->usuario->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('usuario'))
                                <span class="help-block" role="alert">{{ $errors->first('usuario') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.estudiante.fields.usuario_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                            <label for="curso_id">{{ trans('cruds.estudiante.fields.curso') }}</label>
                            <select class="form-control select2" name="curso_id" id="curso_id">
                                @foreach($cursos as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('curso_id') ? old('curso_id') : $estudiante->curso->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('curso'))
                                <span class="help-block" role="alert">{{ $errors->first('curso') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.estudiante.fields.curso_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection