@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.materium.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.materia.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                            <label class="required" for="nombre">{{ trans('cruds.materium.fields.nombre') }}</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre', '') }}" required>
                            @if($errors->has('nombre'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.materium.fields.nombre_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                            <label for="curso_id">{{ trans('cruds.materium.fields.curso') }}</label>
                            <select class="form-control select2" name="curso_id" id="curso_id">
                                @foreach($cursos as $id => $entry)
                                    <option value="{{ $id }}" {{ old('curso_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('curso'))
                                <span class="help-block" role="alert">{{ $errors->first('curso') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.materium.fields.curso_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection