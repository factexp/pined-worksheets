@extends('layouts.admin')
@section('content')
<div class="content">
    @can('tarea_estudiante_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route('admin.tarea-estudiantes.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.tareaEstudiante.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.tareaEstudiante.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-TareaEstudiante">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        {{ trans('cruds.tareaEstudiante.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.tareaEstudiante.fields.tarea') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.tareaEstudiante.fields.valor') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.tareaEstudiante.fields.estudiante') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.tareaEstudiante.fields.profesor') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.tareaEstudiante.fields.curso') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.tareaEstudiante.fields.materia') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tareaEstudiantes as $key => $tareaEstudiante)
                                    <tr data-entry-id="{{ $tareaEstudiante->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $tareaEstudiante->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $tareaEstudiante->tarea->nombre ?? '' }}
                                        </td>
                                        <td>
                                            {{ $tareaEstudiante->valor ?? '' }}
                                        </td>
                                        <td>
                                            {{ $tareaEstudiante->estudiante->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $tareaEstudiante->profesor->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $tareaEstudiante->curso->nombre ?? '' }}
                                        </td>
                                        <td>
                                            {{ $tareaEstudiante->materia->nombre ?? '' }}
                                        </td>
                                        <td>
                                            @can('tarea_estudiante_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('admin.tarea-estudiantes.show', $tareaEstudiante->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('tarea_estudiante_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.tarea-estudiantes.edit', $tareaEstudiante->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('tarea_estudiante_delete')
                                                <form action="{{ route('admin.tarea-estudiantes.destroy', $tareaEstudiante->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('tarea_estudiante_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.tarea-estudiantes.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-TareaEstudiante:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection