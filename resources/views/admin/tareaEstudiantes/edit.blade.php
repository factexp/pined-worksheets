@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.tareaEstudiante.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.tarea-estudiantes.update", [$tareaEstudiante->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('tarea') ? 'has-error' : '' }}">
                            <label class="required" for="tarea_id">{{ trans('cruds.tareaEstudiante.fields.tarea') }}</label>
                            <select class="form-control select2" name="tarea_id" id="tarea_id" required>
                                @foreach($tareas as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('tarea_id') ? old('tarea_id') : $tareaEstudiante->tarea->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('tarea'))
                                <span class="help-block" role="alert">{{ $errors->first('tarea') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.tareaEstudiante.fields.tarea_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('valor') ? 'has-error' : '' }}">
                            <label for="valor">{{ trans('cruds.tareaEstudiante.fields.valor') }}</label>
                            <input class="form-control" type="text" name="valor" id="valor" value="{{ old('valor', $tareaEstudiante->valor) }}">
                            @if($errors->has('valor'))
                                <span class="help-block" role="alert">{{ $errors->first('valor') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.tareaEstudiante.fields.valor_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection