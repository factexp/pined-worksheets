@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.tarea.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.tareas.store") }}" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="form-group {{ $errors->has('materia') ? 'has-error' : '' }}">
                            <label for="materia_id">{{ trans('cruds.tarea.fields.materia') }}</label>
                            <select class="form-control select2" name="materia_id" id="materia_id">
                                @foreach($materias as $id => $entry)
                                    <option value="{{ $id }}" {{ old('materia_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('materia'))
                                <span class="help-block" role="alert">{{ $errors->first('materia') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.tarea.fields.materia_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                            <label for="curso_id">{{ trans('cruds.tarea.fields.curso') }}</label>
                            <select class="form-control select2" name="curso_id" id="curso_id">
                                @foreach($cursos as $id => $entry)
                                    <option value="{{ $id }}" {{ old('curso_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('curso'))
                                <span class="help-block" role="alert">{{ $errors->first('curso') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.tarea.fields.curso_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                            <label class="required" for="nombre">{{ trans('cruds.tarea.fields.nombre') }}</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre', '') }}" required>
                            @if($errors->has('nombre'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.tarea.fields.nombre_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('archivo') ? 'has-error' : '' }}">
                            <label for="archivo">{{ trans('cruds.tarea.fields.archivo') }}</label>
                            <div class="needsclick dropzone" id="archivo-dropzone">
                            </div>
                            @if($errors->has('archivo'))
                                <span class="help-block" role="alert">{{ $errors->first('archivo') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.tarea.fields.archivo_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    Dropzone.options.archivoDropzone = {
    url: '{{ route('admin.tareas.storeMedia') }}',
    maxFilesize: 10, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10
    },
    success: function (file, response) {
      $('form').find('input[name="archivo"]').remove()
      $('form').append('<input type="hidden" name="archivo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="archivo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($tarea) && $tarea->archivo)
      var file = {!! json_encode($tarea->archivo) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="archivo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection