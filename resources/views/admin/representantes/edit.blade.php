@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.representante.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.representantes.update", [$representante->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('estudiante') ? 'has-error' : '' }}">
                            <label for="estudiante_id">{{ trans('cruds.representante.fields.estudiante') }}</label>
                            <select class="form-control select2" name="estudiante_id" id="estudiante_id">
                                @foreach($estudiantes as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('estudiante_id') ? old('estudiante_id') : $representante->estudiante->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('estudiante'))
                                <span class="help-block" role="alert">{{ $errors->first('estudiante') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.representante.fields.estudiante_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('representante') ? 'has-error' : '' }}">
                            <label for="representante_id">{{ trans('cruds.representante.fields.representante') }}</label>
                            <select class="form-control select2" name="representante_id" id="representante_id">
                                @foreach($representantes as $id => $entry)
                                    <option value="{{ $id }}" {{ (old('representante_id') ? old('representante_id') : $representante->representante->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('representante'))
                                <span class="help-block" role="alert">{{ $errors->first('representante') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.representante.fields.representante_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection